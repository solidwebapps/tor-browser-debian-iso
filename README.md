# tor-browser-debian-iso

Custom ISO using Debian 11 with Tor Browser 12.0.2.

For increased privacy, use the ISO in a virtual machine.

User name and password as "user".